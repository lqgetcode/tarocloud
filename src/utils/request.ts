import Taro from '@tarojs/taro'

const Api = (name: string, data: object): Promise<object> => {
  return Taro.cloud.callFunction({
    name,
    data,
  })
}

export default Api
