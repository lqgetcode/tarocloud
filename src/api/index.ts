import Api from '../utils/request'

// 登录获取openid
const login = (data: object ={}): Promise<object> => Api('login', data)

// 新增数据
const add = (data: object ={}): Promise<object> => Api('settodo', data)

// 查询数据
const query = (data: object ={}): Promise<object> => Api('gettodo', data)

// 修改数据
const update = (data: object ={}): Promise<object> => Api('update', data)

// 删除数据
const remove = (data: object ={}): Promise<object> => Api('remove', data)

export default {
  login,
  add,
  query,
  update,
  remove
}
