import { Component } from 'react'
import Taro from '@tarojs/taro'
import './app.scss'

class App extends Component {

  componentDidMount () {}

  componentDidShow () {}

  componentDidHide () {}

  componentDidCatchError () {}
  // this.props.children 是将要会渲染的页面
  render () {
    // 
    Taro.cloud.init({
        env: 'clond-3g83jsg14b0e13a8'
    })
    return this.props.children
  }
}

export default App
