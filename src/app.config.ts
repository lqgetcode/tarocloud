export default {
  pages: [
    'pages/index/index',
    'pages/category/index',
    'pages/cart/index',
    'pages/mine/index'
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
  tabBar: {
    "backgroundColor": "#fafafa",
    "borderStyle": "white",
    "selectedColor": "#AB956D",
    "color": "#666",
    "list": [
      {
        "pagePath": "pages/index/index",
        "iconPath": "./images/home.png",
        "selectedIconPath": "./images/home@selected.png",
        "text": "首页"
      },
      {
        "pagePath": "pages/category/index",
        "iconPath": "./images/category.png",
        "selectedIconPath": "./images/category@selected.png",
        "text": "分类"
      },
      {
        "pagePath": "pages/cart/index",
        "iconPath": "./images/cart.png",
        "selectedIconPath": "./images/cart@selected.png",
        "text": "购物车"
      },
      {
        "pagePath": "pages/mine/index",
        "iconPath": "./images/my.png",
        "selectedIconPath": "./images/my@selected.png",
        "text": "我的"
      }
    ]
  }
}
